# INDEXER UN FICHIER
git add [nomficher]

# INDEXER TOUS LES FICHIERs DU DOSSIER COURRANT ET SES ENFANTS
git add .

# COMMITER SUR LA BRANCHE EN LOCAL
git commit -m "[messageobligatoire]"

# POUR POUSSER SUR BITBUCKET
git push 

# RESTAUTATION A LA Tête de version
git reset --hard HEAD

# CREER BRANCHE 
git branch feat-exemple +  git push --set-upstream origin feat-exemple

# SWITCH BRANCH
git checkout master/feat-exemple
ou
git checkout feat-exemple

# VOIR L'HISTORIQUE
GIT LOG

# voir les modifications courantes (indéxé en vert et non indéxé en rouge)
git status
